import axios from "./../axios"

export default {
  namespaced: true,
  state: {
    auth: null,
    user: null
  },
  getters: {
    authenticated(state) {
      return state.auth && state.user
    },
    user(state) {
      return state.user
    }
  },
  mutations: {
    SET_AUTH(state, auth) {
      state.auth = auth
    },
    SET_USER(state, data) {
      state.user = data
    }
  },
  actions: {
    async register({
      dispatch
    }, credentials) {
      const response = await axios.post('register', credentials)
      return dispatch('attempt', response)
    },

    async signIn({
      dispatch
    }, credentials) {
      const response = await axios.post('login', credentials)
      return dispatch('attempt')
    },

    async attempt({
      commit,
      state
    }) {
      try {
        const response = await axios.get('api/user')
        commit('SET_AUTH', true)
        commit('SET_USER', response.data)
        localStorage.setItem('auth', 'true')
      } catch (e) {
        commit('SET_AUTH', null)
        commit('SET_USER', null)
        localStorage.removeItem('auth', 'true')
      }

        if (!state.auth) {
          return
        }
    },

    signOut({
      commit
    }) {
      return axios.post('logout').then(() => {
        commit('SET_AUTH', null)
        commit('SET_USER', null)
      })
    }
  }
}
