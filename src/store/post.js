import axios from "./../axios"

export default {
  namespaced: true,
  state: {
    posts: null,
    user_posts: null,
    message: null
  },
  getters: {
    posts(state) {
      return state.posts
    },
    message(state) {
      return state.message
    },
    user_posts(state) {
      return state.user_posts
    }
  },
  mutations: {
    SET_POSTS(state, posts) {
      state.posts = posts
    },
    SET_USER_POSTS(state, user_posts) {
      state.user_posts = user_posts
    },
    SET_MESSAGE(state, message) {
      state.message = message
    }
  },

  actions: {
    async getPosts({
      commit
    }) {
      const response = await axios.get('api/posts')
      console.log(response.data)
      commit('SET_POSTS', response.data)
    },
    async getUserPosts({
      commit
    }) {
      const response = await axios.get('api/user/posts')
      console.log(response.data)
      commit('SET_USER_POSTS', response.data)
    },

    async create({
      commit
    }) {
      const response = await axios.post('api/posts')
      commit('SET_MESSAGE', response.message)
    }
  }
}
