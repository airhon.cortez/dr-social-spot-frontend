import axios from "./../axios"

export default {
  namespaced: true,
  state: {
    user: null,
    message: null,
    error_messages: null,
  },
  getters: {
    message(state) {
      return state.message
    },
    error_messages(state) {
      return state.error_messages
    },
    user(state) {
      return state.user
    }
  },
  mutations: {

    SET_USER(state, user) {
      state.user = user
    },
    SET_MESSAGE(state, message) {
      state.message = message
    },
    SET_ERROR_MESSAGE(state, error_messages) {
      state.error_messages = error_messages
    }
  },

  actions: {
    async update({
      commit
    }, form) {
      await axios.post('api/profile/edit', form).then((response) => {
        commit('SET_MESSAGE', response.data.message)
      }).catch((error) => {
        commit('SET_ERROR_MESSAGE', 'Invalid Inputs.')
      })

    },

  }
}
